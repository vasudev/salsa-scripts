#!/bin/sh

set -eu

usage() {
    cat <<EOF
    Usage: $0 alioth-project git-repo salsa-group

        alioth-project  Project group on anonscm.debian.org/git/
        git-repo        Repository under alioth-project to import
        salsa-group     Group under Salsa where we need to import alioth-project/git-repo

EOF
}

if [ "$#" -ne 3 ] || [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    usage
    exit 1
fi

case "$2" in
    *.git)
        REPO_NAME="$2"
        ;;
    *)
        REPO_NAME="${2}.git"
        ;;
esac

ALIOTH_URL="https://anonscm.debian.org/git"
OLD_REPO_URL="${ALIOTH_URL}/$1/${REPO_NAME}"
SALSA_GROUP="$3"
REPO_NAME="${OLD_REPO_URL##*/}"
PROJECT="${REPO_NAME%.git}"
DESCRIPTION="$PROJECT packaging"

. ./helper

SALSA_GROUP_ID=$(get_group_id "$SALSA_GROUP")
[ -n "$SALSA_GROUP_ID" ] || err "$SALSA_GROUP not found. Did you gave correct group name?"
ensure import_git_project "$PROJECT" "$SALSA_GROUP_ID" "$DESCRIPTION" "$OLD_REPO_URL"
echo "You may now browse your project on: ${SALSA_URL%/api*}/$SALSA_GROUP/$PROJECT"
