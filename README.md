For the lazy people like me, here are a few simple scripts to interact with
gitlab's API and setup projects on Salsa (salsa.debian.org).

* import.sh https://anonscm.debian.org/git/pkg-foo/bar.git pkg-baz

  Creates a new project 'bar' in group 'pkg-baz' from an old git repo hosted
in 'https://anonscm.debian.org/git/pkg-foo/bar.git'

* alioth-import pkg-voip biboumi pkg-voip-team

Imports biboumi from https://anonscm.debian.org/git/pkg-voip/biboumi.git to
salsa group https://salsa.debian.org/pkg-voip-team. Where `import.sh` can be
used for importing from any given git URL. `alioth-import` is specific for
importing from Alioth projects.

* irker.sh foo \#debian-bar

  Sets up IRC notifications for project 'foo' on IRC channel '#debian-bar'

* hook_tagpending.sh foo

  Sets up a webhook for project 'foo' which marks bug appearing in commit
messages as 'pending' on Debian BTS.

* hook_close.sh foo

  Sets up a webhook for project 'foo' which closes Debian BTS bugs appearing
in commit messages.

* emails_on_push.sh foo "email addresses"

  Sets up email notifications on push for project 'foo'. Emails will be sent
  to "email addresses" (this second argument can be omitted, in which case
  emails will be sent to the Debian Package Tracker through
  dispatch@tracker.debian.org).

* list_projects.sh pkg-baz

  Lists on stdout all the projects of the group pkg-baz, one project per line.
  The list will contain at least public projects, and also private projects to
  which the token gives access to.

In order to use these scripts, you must create a 'salsarc' file with the
following content:

	SALSA_URL="https://salsa.debian.org/api/v4"
	SALSA_TOKEN="...."

You may create API tokens by visiting your [personal access tokens](https://salsa.debian.org/profile/personal_access_tokens)
page.

Documentation of Salsa service can be found [here](https://wiki.debian.org/Salsa/Doc)
