#!/bin/bash
# See https://wiki.debian.org/Salsa/Doc#Dealing_with_Debian_BTS_from_commit_messages

set -eu

. ./salsarc

if [ "$#" -ne 1 ] || [ -z "$1" ]; then
    echo "Usage: $0 pkg-foo/bar" >&2
    echo "  where 'pkg-foo/bar' is the Salsa project name for which you want to setup the webhook" >&2
    exit 1
fi

PROJECT_NAME="$1"
SRC_NAME="${PROJECT_NAME##*/}"
PROJECT_PATH="${PROJECT_NAME//\//%2F}"
CLOSE_URL="https://webhook.salsa.debian.org/close/$SRC_NAME"

PROJECT_ID=$(curl --silent -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" "$SALSA_URL/projects/$PROJECT_PATH" | jq '.id')
if [ -z "$PROJECT_ID" ]; then
    echo "Project $PROJECT_NAME not found among your owned projects on $SALSA_URL service" >&2
    exit 1
else
    echo "Setting up close webhook for $PROJECT_NAME ($PROJECT_ID)"
fi

ALREADY_HOOKED=$(curl --silent -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" $SALSA_URL/projects/$PROJECT_ID/hooks | jq ".[] | select(.url == \"$CLOSE_URL\") | .id")

case $ALREADY_HOOKED in
    [0-9]*) echo "close service is already configured for $PROJECT_NAME" ;;
    *) curl -XPOST --header "PRIVATE-TOKEN: $SALSA_TOKEN" \
	$SALSA_URL/projects/$PROJECT_ID/hooks \
	--data "url=$CLOSE_URL&push_events=1&enable_ssl_verification=1"
       if [ $? -eq 0 ]; then
           echo
           echo "All done."
       else
           echo
           echo "Something went wrong!"
       fi;;
esac
